#!/bin/sh

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build main.go

mv main aes_linux

CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build main.go

mv main aes_mac

CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build main.go

mv main.exe aes_win.exe


