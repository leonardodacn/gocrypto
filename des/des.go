package des

import (
	"bytes"
	"crypto/cipher"
	"crypto/des"
	"encoding/base64"
	"fmt"
)

func TestDes() {
	fmt.Println("des test start")
	key := []byte("VFNSMzQ1")
	result, err := DesEncrypt([]byte("polaris@studygolang"), key)
	if err != nil {
		panic(err)
	}
	fmt.Println(result)
	result = "p6Ip/PYbnJY="
	origData, err := DesDecrypt([]byte(result), key)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(origData))
	fmt.Println("des test end")
}

func Test3Des() {
	key := []byte("VFNSMzQ1Njc4OTAxMjM0NTY3")
	result, err := TripleDesEncrypt([]byte("root"), key)
	if err != nil {
		panic(err)
	}
	fmt.Println(result)
	//result = "p6Ip/PYbnJY="
	if origData, err := TripleDesDecrypt([]byte(result), key); err == nil {
		fmt.Println(string(origData))
	} else {
		panic(err)
	}
}

func DesEncrypt(origData, key []byte) (string, error) {
	block, err := des.NewCipher(key)
	if err != nil {
		return "", err
	}
	origData = PKCS5Padding(origData, block.BlockSize())
	// origData = ZeroPadding(origData, block.BlockSize())
	blockMode := cipher.NewCBCEncrypter(block, key)
	crypted := make([]byte, len(origData))
	// 根据CryptBlocks方法的说明，如下方式初始化crypted也可以
	// crypted := origData
	blockMode.CryptBlocks(crypted, origData)
	if result := base64.StdEncoding.EncodeToString(crypted); result != "" {
		return result, nil
	}
	return "", nil
}

func DesDecrypt(crypted, key []byte) (string, error) {
	if data, err := base64.StdEncoding.DecodeString(string(crypted)); err == nil {
		crypted = []byte(data)
	} else {
		return "", err
	}
	block, err := des.NewCipher(key)
	if err != nil {
		return "", err
	}
	blockMode := cipher.NewCBCDecrypter(block, key)
	origData := make([]byte, len(crypted))
	// origData := crypted
	blockMode.CryptBlocks(origData, crypted)
	origData = PKCS5UnPadding(origData)
	// origData = ZeroUnPadding(origData)
	return string(origData), nil
}

// 3DES加密
func TripleDesEncrypt(origData, key []byte) (string, error) {
	block, err := des.NewTripleDESCipher(key)
	if err != nil {
		return "", err
	}
	origData = PKCS5Padding(origData, block.BlockSize())
	// origData = ZeroPadding(origData, block.BlockSize())
	blockMode := cipher.NewCBCEncrypter(block, key[:8])
	crypted := make([]byte, len(origData))
	blockMode.CryptBlocks(crypted, origData)
	if result := base64.StdEncoding.EncodeToString(crypted); result != "" {
		return result, nil
	}
	return "", nil
}

// 3DES解密
func TripleDesDecrypt(crypted, key []byte) (string, error) {
	if data, err := base64.StdEncoding.DecodeString(string(crypted)); err == nil {
		crypted = []byte(data)
	} else {
		return "", err
	}
	block, err := des.NewTripleDESCipher(key)
	if err != nil {
		return "", err
	}
	blockMode := cipher.NewCBCDecrypter(block, key[:8])
	origData := make([]byte, len(crypted))
	// origData := crypted
	blockMode.CryptBlocks(origData, crypted)
	origData = PKCS5UnPadding(origData)
	// origData = ZeroUnPadding(origData)
	return string(origData), nil
}

func ZeroPadding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{0}, padding)
	return append(ciphertext, padtext...)
}

func ZeroUnPadding(origData []byte) []byte {
	return bytes.TrimRightFunc(origData, func(r rune) bool {
		return r == rune(0)
	})
}

func PKCS5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func PKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	// 去掉最后一个字节 unpadding 次
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}
